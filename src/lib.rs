use std::fs::{create_dir_all, File};
use std::io::Write;
use std::process::Command;
use regex::Regex;
use tera::{Context, Tera};

pub struct Glaucous {
    tera: Tera,
    input_path: String,
    output_path: String,
    pub context: Context,
    pub dry_run: bool,
}

impl Glaucous {
    pub fn new(input_path: String, output_path: String) -> Glaucous {
        let trim = |string: &String| {
            string
                .trim_end_matches(&['/', ' ', '\\'] as &[_])
                .to_string()
        };
        let output_path = trim(&output_path);
        let input_path = trim(&input_path);

        let mut tera = match Tera::new((input_path.clone() + "/**/*.html").as_str()) {
            Ok(t) => t,
            Err(e) => {
                println!("Parsing error(s): {}", e);
                std::process::exit(1);
            }
        };

        tera.autoescape_on(vec![]);

        Glaucous {
            tera,
            input_path,
            output_path,
            context: Context::default(),
            dry_run: false,
        }
    }

    /// Renders all specified .html files, and simply copies all files with other extensions.
    /// ```no_run
    /// use glaucous::Glaucous;
    /// let mut glauc = Glaucous::new("static/".to_string(), "public/".to_string());
    /// let pages = vec![
    ///     // Files with the .html extension will be rendered
    ///     String::from("en/index.html"),
    ///     String::from("en/test.html"),
    ///     // Any file without the .html extension will get copied
    ///     String::from("style.css"),
    ///     // Any directory will get recursively copied
    ///     String::from("assets"),
    /// ];
    /// glauc.context.insert("kekw", "<img src=\"assets/emotes/kekw.webp\"/>");
    /// glauc.render_website(&pages);
    /// ```
    pub fn render_website(&mut self, files: &Vec<String>) {
        for path in files {
            let input_full_path = self.input_path.clone() + "/" + path.as_str();
            let output_full_path = self.output_path.clone() + "/" + path.as_str();

            let re = Regex::new(r".*[/]").unwrap();
            let file_directory = re.find(output_full_path.as_str()).unwrap().as_str();
            create_dir_all(file_directory).unwrap();

            if path.contains(".html") {
                let root_rxp = Regex::new(r"[/]").unwrap();
                let dir_count = root_rxp.find_iter(path.as_str()).collect::<Vec<_>>().len();
                let root_str = String::from("../").repeat(dir_count);

                self.context.insert("to_root", root_str.as_str());

                match self.tera.render(path.as_str(), &self.context) {
                    Ok(content) => {
                        if self.dry_run {
                            println!("{}", output_full_path);
                        } else {
                            let mut file = File::create(output_full_path).unwrap();
                            file.write_all(content.as_bytes())
                                .expect("Failed to write to file");
                        }
                    }
                    Err(e) => panic!("{}", e),
                }
            } else {
                println!("Copying file {}", input_full_path);
                Command::new("cp")
                    .arg("-r")
                    .arg(&input_full_path)
                    .arg(&output_full_path)
                    .status().unwrap();
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::Glaucous;
    use std::fs::*;
    use std::path::Path;

    fn setup() {
        create_dir_all("static/en/lol").unwrap();
        File::create("static/xd.html").unwrap();
        File::create("static/en/index.html").unwrap();
        File::create("static/en/lol/test.html").unwrap();
        File::create("static/style.css").unwrap();
    }

    fn teardown() {
        remove_dir_all("static/").unwrap();
        remove_dir_all("public/").unwrap();
    }

    #[test]
    fn render_all_files_in_input() {
        setup();

        let input_path = "static/".to_string();
        let mut glauc = Glaucous::new(input_path.clone(), "public/".to_string());

        let pages = vec![
            String::from("xd.html"),
            String::from("en/index.html"),
            String::from("en/lol/test.html"),
            String::from("style.css"),
        ];

        // glauc.dry_run = true;
        glauc.render_website(&pages);

        assert!(Path::new("public/en/index.html").exists());
        assert!(Path::new("public/en/lol/test.html").exists());
        assert!(Path::new("public/style.css").exists());

        teardown();
    }
}
